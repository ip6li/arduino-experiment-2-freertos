# Arduino Experiment FreeRTOS

FreeRTOS is a very small kernel, which provides functions for preemtive multitasking
and some interprocess communication. It is really impressive, what an Arduino is able to
do with such a kernel.

# loop()

FreeRTOS requires that this function is empty. FreeRTOS uses "Tasks" for its business.

# Tasks

TaskDigitalRead() and TaskAnalogRead() are taken from FreeRTOS example code.
TaskBlink() let internal LED blinking. Thats Arduinos "Hello world!".
Blinking LED shows, that FreeRTOS does its job.
TaskLCD() shows a dynamic text on a NHD-0216K3Z-FL-GBW display.

Later LED can be used e.g. as visualized watchdog.

# Display

For tests, a Newhaven display NHD-0216K3Z-FL-GBW was used.
Datasheet: See docs/NHD-0216K3Z-FL-GBW.pdf

## Connections

| Display P2 | Name     | Arduino |
| ---------- | -------- | ------- |
| 1          | SPISS    | 11      |
| 2          | SDO      | NN      |
| 3          | SCK/SCL  | 12      |
| 4          | SDI/SDA  | 10      |
| 5          | VSS      | GND     |
| 6          | VDD      | +5V     |

Do not use port 13 if you need to control internal Arduino LED.

# Platform.io Settings

For using STK500 board for programming, following settings
in Platform.io are needed:

![platform.io settings](/docs/platformio.stk500.png)

Please note '-e' is required to avoid the infamous "verification error".

## Arduino bootloader

With STK500 as programmer, Arduino does not need bootloader.

# Network

Arduino Uno (old version) should not to be connected to a network directly due to
security reasons. It is simply impossible to compile a TLS library like
OpenSSL on such a platform.

Even if you are using Arduino Uno Rev. 4 think twice to connect it to a network.
Secure networks do not exists, every network is insecure, even LAN.

## Solution

If you need some type of networking so connect your Arduino Uno by USB to
a Raspberry PI. PI should be configured with latest Raspian and
its security updates. Install security patches daily (cron-apt is your friend).
Even an old Raspi 1 is better than connecting an Arduino to network.

Access to (Web) service on Raspberry should be
always secured by mTLS (that means client certificates). Enforce
mTLS by Nginx/Apache httpd configuration. Do not rely only on WPA2/3, 802.1X etc.

Then let talk a e.g. PHP script to serial interface.

# References

https://microcontrollerslab.com/use-freertos-arduino/
