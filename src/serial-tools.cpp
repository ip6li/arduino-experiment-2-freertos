#include "serial-tools.hpp"

#define defaultSpeed 115200

void clearScreen(void)
{
  // VT102/ANSI clear screen
  Serial.print("\x1B[2J");
}

void setSpeed(void)
{
    Serial.begin(defaultSpeed);
}

void setSpeed(int speed)
{
    Serial.begin(speed);
}
