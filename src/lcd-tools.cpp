#include "lcd-tools.hpp"

void lcdPrintAt(nhdlcd lcd, char *txt, uint8_t line, uint8_t col)
{
  uint8_t pos;

  switch (line)
  {
  case 0:
    pos = 0x00 + col;
    break;
  
  case 1:
    pos = 0x40 + col;
    break;

  default:
    Serial.println("lcdPrintAt: line parameter must be '0' or '1'");
    return;
  }

  lcd.write(Command::SetCursor,pos);
  lcd.write(txt);
}
