#include <Arduino.h>
#include <Arduino_FreeRTOS.h>
#include <semphr.h> // add the FreeRTOS functions for Semaphores (or Flags).
#include <timers.h>

#include "nhdlcd.hpp"
#include "lcd-tools.hpp"
#include "serial-tools.hpp"

#define configTOTAL_HEAP_SIZE ((size_t)(75 * 1024))

/* Co-routine definitions. */
#define configUSE_CO_ROUTINES (0)
#define configMAX_CO_ROUTINE_PRIORITIES (2)

// App Config
#define LED_PORT LED_BUILTIN
#define mainAUTO_RELOAD_TIMER_PERIOD_LED pdMS_TO_TICKS(500)
#define mainAUTO_RELOAD_TIMER_PERIOD_SERIAL pdMS_TO_TICKS(1000)

// Declare a mutex Semaphore Handle which we will use to manage the Serial Port.
// It will be used to ensure only one Task is accessing this resource at any time.
SemaphoreHandle_t xSerialSemaphore;

TimerHandle_t xAutoReloadTimerLED, xAutoReloadTimerSerial;
BaseType_t xTimerLEDStarted, xTimerSerialStarted;

// init LCD ports
#define dataPin (uint8_t)10
#define clockPin (uint8_t)12
#define slaveSelectPin (uint8_t)11
static nhdlcd lcd(dataPin, clockPin, slaveSelectPin);

// define Tasks
void TaskDigitalRead(void *pvParameters);
void TaskAnalogRead(void *pvParameters);
static void TimerLedBlink(TimerHandle_t xTimer);
static void TimerSerialOut(TimerHandle_t xTimer);
void TaskLCD(void *pvParameters);

void setupTimers()
{
  /* Create the auto-reload timer, storing the handle to the created timer in xAutoReloadTimer. */
  xAutoReloadTimerLED = xTimerCreate(
      /* Text name for the software timer - not used by FreeRTOS. */
      "AutoReload",
      /* The software timer's period in ticks. */
      mainAUTO_RELOAD_TIMER_PERIOD_SERIAL,
      /* Setting xAutoRealod to pdTRUE creates an auto-reload timer. */
      pdTRUE,
      /* This example does not use the timer id. */
      0,
      /* The callback function to be used by the software timer being created. */
      TimerSerialOut);

  xAutoReloadTimerSerial = xTimerCreate(
      /* Text name for the software timer - not used by FreeRTOS. */
      "AutoReload",
      /* The software timer's period in ticks. */
      mainAUTO_RELOAD_TIMER_PERIOD_LED,
      /* Setting xAutoRealod to pdTRUE creates an auto-reload timer. */
      pdTRUE,
      /* This example does not use the timer id. */
      0,
      /* The callback function to be used by the software timer being created. */
      TimerLedBlink);

  if ((xAutoReloadTimerLED != NULL) && (xAutoReloadTimerSerial != NULL))
  {
    /* Start the software timers, using a block time of 0 (no block time). The scheduler has
    not been started yet so any block time specified here would be ignored anyway. */
    xTimerLEDStarted = xTimerStart(xAutoReloadTimerLED, 0);
    xTimerSerialStarted = xTimerStart(xAutoReloadTimerSerial, 0);
    /* The implementation of xTimerStart() uses the timer command queue, and xTimerStart()
    will fail if the timer command queue gets full. The timer service task does not get
    created until the scheduler is started, so all commands sent to the command queue will
    stay in the queue until after the scheduler has been started. Check both calls to
    xTimerStart() passed. */

    if ((xTimerLEDStarted == pdPASS) && (xTimerSerialStarted == pdPASS))
    {
      /* Start the scheduler. */
      vTaskStartScheduler();
    }
  }
}

void setupTasks()
{
  // Now set up multiple Tasks to run independently.
  xTaskCreate(
      TaskDigitalRead, "DigitalRead" // A name just for humans
      ,
      128 // This stack size can be checked & adjusted by reading the Stack Highwater
      ,
      NULL // Parameters for the task
      ,
      2 // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
      ,
      NULL); // Task Handle

  xTaskCreate(
      TaskAnalogRead, "AnalogRead" // A name just for humans
      ,
      128 // Stack size
      ,
      NULL // Parameters for the task
      ,
      1 // Priority
      ,
      NULL); // Task Handle

  xTaskCreate(
      TaskLCD, "LCD" // A name just for humans
      ,
      128 // Stack size
      ,
      NULL // Parameters for the task
      ,
      1 // Priority
      ,
      NULL); // Task Handle
}

void setupSemaphores()
{
  // Semaphores are useful to stop a Task proceeding, where it should be paused to wait,
  // because it is sharing a resource, such as the Serial port.
  // Semaphores should only be used whilst the scheduler is running, but we can set it up here.
  if (xSerialSemaphore == NULL) // Check to confirm that the Serial Semaphore has not already been created.
  {
    xSerialSemaphore = xSemaphoreCreateMutex(); // Create a mutex semaphore we will use to manage the Serial Port
    if ((xSerialSemaphore) != NULL)
      xSemaphoreGive((xSerialSemaphore)); // Make the Serial Port available for use, by "Giving" the Semaphore.
  }
}

void setupLCD()
{
    lcd.write(Command::BlinkingCursorOff);
    lcd.write(Command::UnderlineCursorOff);
    lcd.write(Command::ClearScreen);
}

void setup()
{
  setSpeed();

  while (!Serial)
  {
    // wait for serial port to connect. 
    // Needed for native USB, on LEONARDO, MICRO, YUN, 
    // and other 32u4 based boards.
    vTaskDelay(1);
  }

  pinMode(LED_PORT, OUTPUT);

  clearScreen();

  setupLCD();

  setupSemaphores();

  setupTasks();

  setupTimers();
}

void loop()
{
  // Empty. Things are done in Tasks.
  // no delay or other commands!
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/

void TaskDigitalRead(void *pvParameters __attribute__((unused))) // This is a Task.
{
  /*
    DigitalReadSerial
    Reads a digital input on pin 2, prints the result to the serial monitor

    This example code is in the public domain.
  */

  char buffer[17];

  // digital pin 2 has a pushbutton attached to it. Give it a name:
  uint8_t pushButton = 2;

  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);

  for (;;) // A Task shall never return or exit.
  {
    // read the input pin:
    int buttonState = digitalRead(pushButton);

    // See if we can obtain or "Take" the Serial Semaphore.
    // If the semaphore is not available, wait 5 ticks of the Scheduler to see if it becomes free.
    if (xSemaphoreTake(xSerialSemaphore, (TickType_t)5) == pdTRUE)
    {
      sprintf(buffer, "D: %3d", buttonState);
      lcdPrintAt(lcd, buffer, 0, 8);

      xSemaphoreGive(xSerialSemaphore); // Now free or "Give" the Serial Port for others.
    }

    vTaskDelay(1); // one tick delay (15ms) in between reads for stability
  }
}

void TaskAnalogRead(void *pvParameters __attribute__((unused))) // This is a Task.
{
  char buffer[17];

  for (;;)
  {
    // read the input on analog pin 0:
    int sensorValue = analogRead(A0);

    // See if we can obtain or "Take" the Serial Semaphore.
    // If the semaphore is not available, wait 5 ticks of the Scheduler to see if it becomes free.
    if (xSemaphoreTake(xSerialSemaphore, (TickType_t)5) == pdTRUE)
    {
      sprintf(buffer, "A: %3d", sensorValue);
      lcdPrintAt(lcd, buffer, 0, 0);

      xSemaphoreGive(xSerialSemaphore); // Now free or "Give" the Serial Port for others.
    }

    vTaskDelay(1); // one tick delay (15ms) in between reads for stability
  }
}

void TaskLCD(void *pvParameters __attribute__((unused))) // This is a Task.
{
  static int counter = 0;
  char strCounter[32];

  TickType_t xLastWakeTime = xTaskGetTickCount();
 
  for (;;)
  {
    sprintf(strCounter, "LCD Count: %5d", counter);
    lcdPrintAt(lcd, strCounter, 1, 0);

    counter++;

    xTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(1000));
  }
}

/*--------------------------------------------------*/
/*--------------------- Timers ---------------------*/
/*--------------------------------------------------*/

static void TimerLedBlink(TimerHandle_t xTimer) // This is a timer callback.
{
  static bool ledStateTimer;

  if (ledStateTimer)
  {
    digitalWrite(LED_PORT, LOW);
  }
  else
  {
    digitalWrite(LED_PORT, HIGH);
  }

  ledStateTimer = !ledStateTimer;
}

static void TimerSerialOut(TimerHandle_t xTimer) // This is a timer callback.
{
  TickType_t xTimeNow = xTaskGetTickCount();

  Serial.print("Auto-reload timer callback executing ");
  Serial.print(xTimeNow/31);
  Serial.print("\r");
}
